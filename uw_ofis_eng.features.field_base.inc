<?php
/**
 * @file
 * uw_ofis_eng.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_ofis_eng_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_block'.
  $field_bases['field_block'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_block',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'moddelta' => array(
        0 => 'moddelta',
      ),
    ),
    'locked' => 0,
    'module' => 'blockreference',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'referenceable_modules' => array(),
    ),
    'translatable' => 0,
    'type' => 'blockreference',
  );

  return $field_bases;
}
