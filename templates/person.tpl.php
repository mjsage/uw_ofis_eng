<div class="expert">
    <h2><?php print $first_name ?>  <?php print $last_name; ?> <span class="expand"><i class="fa fa-plus"></i></span></h2>
    <div class="profile">
        <div class="department">Department of <?php print $department; ?></div>
        <div class="contact">Email: <?php print $email; ?> | Phone: <?php print $phone; ?></div>
    </div>
    <div class="research-interests hidden">
        <p><strong>Research Interests</strong></p>
        <ul>
        <?php
            foreach ($research_interests as $interest) { ?>
            <li><?php print $interest; ?></li>

        <?php }; ?>
        </ul>
        <div class="more">
            <a href="https://uwaterloo.ca/<?php print $department_id; ?>/profile/<?php print $user_id ?>" target="_blank" class="button">Show full profile</a>
        </div>
    </div>
</div>