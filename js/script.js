/**
 * @file Behaviors for searching the OFIS API.
 */

(function ($) {

    Drupal.behaviors.uw_ofis_eng = {
        attach: function (context, settings) {


            function search_experts(){
                inputString = $("#ofis-search").val() ;
                if(inputString.length === 0) {
                    // Hide the suggestion box.
                    $('#autoSuggestionsList').hide();
                } else {
                    $("#ofis-results .waiting").show();
                    $.post("ofis/search/" + inputString, function(data){
                        if(data.length >0) {
                            $('#autoSuggestionsList').show();
                            $('#autoSuggestionsList').html(data);
                            $("#ofis-results .waiting").hide();
                        }
                    });
                }
            }
            $("#ofis-submit").click(function () {
                search_experts();
            });

            $('#ofis-search').keypress(function (e) {
                if (e.which == 13) {
                    search_experts();
                    return false;    //<---- Add this line
                }
            });


            $("#ofis-results").on('click', '.expand', function(el){
                $(this).parent().parent().find(".research-interests").slideToggle();
                $fa = $(this).find(".fa");

                if ($fa.hasClass("fa-plus")) {
                    $fa.removeClass("fa-plus").addClass("fa-minus");
                } else {
                    $fa.removeClass("fa-minus").addClass("fa-plus");
                }
            });
        }
    };

})(jQuery);