<?php
/**
 * @file
 * uw_ofis_eng.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ofis_eng_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_ofis_eng_paragraphs_info() {
  $items = array(
    'ofis_block' => array(
      'name' => 'OFIS block',
      'bundle' => 'ofis_block',
      'locked' => '1',
    ),
  );
  return $items;
}
