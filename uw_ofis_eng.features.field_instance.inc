<?php
/**
 * @file
 * uw_ofis_eng.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_ofis_eng_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'paragraphs_item-ofis_block-field_block'.
  $field_instances['paragraphs_item-ofis_block-field_block'] = array(
    'bundle' => 'ofis_block',
    'default_value' => array(
      0 => array(
        'moddelta' => 'uw_ofis_eng:uw_ofis_eng_search_block',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'blockreference',
        'settings' => array(
          'show_empty_blocks' => FALSE,
        ),
        'type' => 'blockreference_default',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_block',
    'label' => 'OFIS Block',
    'required' => 0,
    'settings' => array(
      'blockreference_modules' => array(
        'admin_language' => 0,
        'aggregator' => 0,
        'biblio' => 0,
        'block' => 0,
        'calendar' => 0,
        'cas' => 0,
        'comment' => 0,
        'context_ui' => 0,
        'devel' => 0,
        'diff' => 0,
        'forward' => 0,
        'gmap_location' => 0,
        'locale' => 0,
        'menu' => 0,
        'node' => 0,
        'responsive_menu_combined' => 0,
        'search' => 0,
        'shortcut' => 0,
        'system' => 0,
        'user' => 0,
        'uw_auth_cas_common' => 0,
        'uw_clear_cache_block' => 0,
        'uw_ct_blog' => 0,
        'uw_ct_contact' => 0,
        'uw_ct_event' => 0,
        'uw_ct_home_page_banner' => 0,
        'uw_ct_news_item' => 0,
        'uw_ct_person_profile' => 0,
        'uw_ct_project' => 0,
        'uw_ct_service' => 0,
        'uw_ct_web_form' => 0,
        'uw_ct_web_page' => 0,
        'uw_nav_global_footer' => 0,
        'uw_nav_site_footer' => 0,
        'uw_ofis_eng' => 'uw_ofis_eng',
        'uw_social_media_sharing' => 0,
        'views' => 0,
        'webform' => 0,
        'workbench' => 0,
      ),
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('OFIS Block');

  return $field_instances;
}
