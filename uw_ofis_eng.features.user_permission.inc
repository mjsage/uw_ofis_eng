<?php
/**
 * @file
 * uw_ofis_eng.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ofis_eng_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer uw_ofis_eng settings'.
  $permissions['administer uw_ofis_eng settings'] = array(
    'name' => 'administer uw_ofis_eng settings',
    'roles' => array(),
    'module' => 'uw_ofis_eng',
  );

  return $permissions;
}
